from socket import *
from ssl import *
from base64 import *

NL = '\r\n' # newline

OK_CODE = '250' # OK message number

SMTP_SERVER = 'smtp.gmail.com'
TLS_PORT = 465

USERNAME = 'angerfloof@gmail.com' # my fake gmail account
PASSWD = 'angerfloofisthedude'

# helper function which encodes a string as base 64
# and returns the string version of the encoding
def base64_encode_str(s):
    return b64encode(s.encode()).decode()

# checks the receipt's beginning code number
# for its equivalence to the passed code
def check_code(receipt, code):
    return receipt[:3] == code

# use these functions to send and recv
def send(s):
    ssl_sock.send(s.encode())
    print(f'SENT:\n{s}')

def recv(s=''):
    s += ssl_sock.recv(1024).decode()
    print(f'RCVD:\n{s}')

# helper function to do the basic dialogue steps
def dialogue(s):
    send(s + NL)
    recv()

# create socket and then SSL socket
sock = socket(AF_INET, SOCK_STREAM)
ssl_sock = wrap_socket(sock, ssl_version=PROTOCOL_SSLv23)

ssl_sock.connect((SMTP_SERVER, TLS_PORT))
# print cipher being used
print(f'{ssl_sock.cipher()}\n')

# send hello command
# this also performs the first recv() which should get a code of 220
dialogue('EHLO localhost')

# here the server sends two messages with a short time gap in-between
# the first is a greeting and the second is a series of 250 messages
# ensure you have received at least 1 250 message before you “speak” again
# to accomplish this, create a loop to verify you’ve received a 250
# and then call the recv() function to receive the rest of the data
receipt = ''
while not check_code(receipt, OK_CODE):
    receipt = ssl_sock.recv(1024).decode()
    print(f'{receipt}\n')

if check_code(receipt, OK_CODE):
    # now that it is your turn in the dialogue “to speak” again...
    # send login command because Google makes you authenticate
    dialogue('AUTH LOGIN')

    # now follow the login prompts...
    dialogue(base64_encode_str(USERNAME)) # send username
    dialogue(base64_encode_str(PASSWD)) # send password

    # now that you are authenticated, you can start the normal SMTP dialogue
    dialogue('MAIL FROM: <angerfloof@gmail.com>')
    dialogue('RCPT TO: <josborne@cedarville.edu>')

    dialogue('DATA')

    send(f'From: James\nTo: josborne@cedarville.edu\n' + 
         f'Reply-To: {USERNAME}\nSubject: Test SMTP Email{NL}')
    dialogue(f'Hey guys it\'s me, your neighborhood grizzly bear{NL}.{NL}')

    # send quit command
    send(f'Quit{NL}')

# close connection
# closing the wrapped socket closes the underlying socket as well
ssl_sock.close()
